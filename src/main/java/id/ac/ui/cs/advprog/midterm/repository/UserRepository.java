package id.ac.ui.cs.advprog.midterm.repository;

import id.ac.ui.cs.advprog.midterm.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {
    long count();
    List<User> findByRole (String role);
}
