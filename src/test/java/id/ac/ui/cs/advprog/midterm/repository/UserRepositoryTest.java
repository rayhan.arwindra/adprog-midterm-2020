package id.ac.ui.cs.advprog.midterm.repository;

import static org.junit.jupiter.api.Assertions.*;
import id.ac.ui.cs.advprog.midterm.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testRepositoryCount(){
        userRepository.save(new User("test","email@example.com", "role"));
        assertEquals(1, userRepository.count());
    }
}
