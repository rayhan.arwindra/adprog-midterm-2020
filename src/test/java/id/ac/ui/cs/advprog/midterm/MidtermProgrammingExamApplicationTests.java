package id.ac.ui.cs.advprog.midterm;

import id.ac.ui.cs.advprog.midterm.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MidtermProgrammingExamApplicationTests {

	@Autowired
	UserController userController;

	@Test
	void contextLoads() {
		assertThat(userController).isNotNull();
	}

	@Test
	public void main() {
		MidtermProgrammingExamApplication.main(new String[] {});
		assertThat(1).isEqualTo(1);
	}


}
