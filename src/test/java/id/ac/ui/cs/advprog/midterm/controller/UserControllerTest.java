package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.model.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.util.NestedServletException;

import javax.validation.Valid;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Mock
    private BindingResult result;
    @Mock
    private Model model;
    @MockBean
    private UserRepository userRepository;

    private static final String INVALID_ID = "Invalid user Id:1";

    private User user;
    @BeforeEach
    public void setUp(){
        user = new User("test","email@email.com","role");
        user.setId(0);
        when(userRepository.findById(user.getId())).thenReturn(java.util.Optional.ofNullable(user));
 }


    @Test
    public void testHomePage() throws Exception{
        mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("No users yet!")));
    }

    @Test
    public void testSignUpForm() throws Exception{
        mockMvc.perform(get("/signup")).andExpect(status().isOk()).andExpect(view().name("add-user"));
    }

    @Test
    public void testAddUserValid() throws Exception{
        HashMap parameter = new HashMap();
        parameter.put("user", user);
        parameter.put("result", result);
        parameter.put("model", model);
        mockMvc.perform(post("/adduser").flashAttrs(parameter)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testAddUserInvalid() throws Exception{
        user.setEmail(null);
        HashMap parameter = new HashMap();
        parameter.put("user", user);
        parameter.put("result", result);
        parameter.put("model", model);
        mockMvc.perform(post("/adduser").flashAttrs(parameter)).andExpect(status().isOk()).andExpect(view().name("add-user"));
    }

    @Test
    public void testUpdateFormValid() throws Exception{
        mockMvc.perform(get("/edit/"+Long.toString(user.getId())))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    public void testUpdateFormInvalid() throws Exception{
        try{
            mockMvc.perform(get("/edit/1"));
        }catch (Exception e){
            assertThat(e).hasMessageContaining(INVALID_ID);
        }
    }

    @Test
    public void testUpdateUserValid() throws Exception{
        HashMap parameters = new HashMap();
        parameters.put("id",user.getId());
        parameters.put("user",user);
        parameters.put("result",result);
        parameters.put("model",model);

        mockMvc.perform(post("/update/"+Long.toString(user.getId())).flashAttrs(parameters)).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testUpdateUserInvalid() throws Exception{
        user.setEmail(null);
        HashMap parameters = new HashMap();
        parameters.put("id",user.getId());
        parameters.put("user",user);
        parameters.put("result",result);
        parameters.put("model",model);

        mockMvc.perform(post("/update/"+Long.toString(user.getId())).flashAttrs(parameters)).andExpect(status().isOk()).andExpect(view().name("update-user"));
    }

    @Test
    public void testDeleteUserValid() throws Exception{
        mockMvc.perform(get("/delete/"+Long.toString(user.getId()))).andExpect(status().isOk()).andExpect(view().name("index"));
    }

    @Test
    public void testDeleteUserInvalid() throws Exception{
        try {
            mockMvc.perform(get("/delete/1"));
        }catch (Exception e){
            assertThat(e).hasMessageContaining(INVALID_ID);
        }
    }

    @Test
    public void testSearchRoleGet() throws Exception {
        mockMvc.perform(get("/search-role")).andExpect(status().isOk()).andExpect(view().name("search-role"));
    }

    @Test
    public void testSearchRolePost() throws Exception{
        mockMvc.perform(post("/search-role")).andExpect(status().isOk()).andExpect(view().name("search-role"));
    }
}
