package id.ac.ui.cs.advprog.midterm.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class UserTest {

    private User user;

    @BeforeEach
    public void setUp(){
        user = new User("Example","test@email.com", "role");
    }
    @Test
    public void testGetId(){
        long testId = user.getId();
        assertEquals(testId, user.getId());
    }
    @Test
    public void testGetName(){
        String testName = "Example";
        assertEquals(testName,user.getName());
    }
    @Test
    public void testGetEmail(){
        String testEmail = "test@email.com";
        assertEquals(testEmail,user.getEmail());
    }

    @Test
    public void testGetRole(){
        String testRole = "role";
        assertEquals(testRole,user.getRole());
    }

    @Test
    public void testSetId(){
        long newId = 2;
        user.setId(newId);
        assertEquals(newId, user.getId());
    }

    @Test
    public void testSetName(){
        String newName = "example";
        user.setName(newName);
        assertEquals(newName, user.getName());
    }

    @Test
    public void testSetEmail(){
        String newEmail = "email@example.com";
        user.setEmail(newEmail);
        assertEquals(newEmail, user.getEmail());
    }

    @Test
    public void testSetRole(){
        String newRole = "Job";
        user.setRole(newRole);
        assertEquals(newRole, user.getRole());
    }

    @Test
    public void testToString(){
        String expectedString = "User{" +
                "id=" + user.getId() +
                ", name='" + user.getName() + '\'' +
                ", email='" + user.getEmail() + '\'' +
                '}';
        assertEquals(expectedString, user.toString());
    }
}
